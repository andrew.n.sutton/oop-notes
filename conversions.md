# Types and safety

The type system of a programming language is the set of types it uses to
a) describe values and b) determine when operations on those values are valid.
Every language has a set of built-in types (e.g., `int`, `bool`), and most
have ways to define new types. C++ has three ways of defining new types:
enumerations, classes, and unions.

A *type error* is kind of bug that results from using a value in a way other
than its original intent, typically by misinterpreting its representation.
For example, early languages did not differentiate between integer and floating
point values (bits are bits). In such languages it was fairly easy to 
misinterpret a floating point value as an integer, with seemingly random bits.

Other kinds of type errors are more subtle. For example, certain conversions
may lose information and cause bugs that way. The Ariane 5 crashed in 1996
as a result of converting a 64-bit double to a 16-bit integer, essentially
truncating the value. Automatically or incorrectly narrowing one set of values
to another is a common source of computation bugs.

Most modern approaches to programming involve the application of the type system
to eliminate or reduce type errors. This property is known as *type safety*.
Type safety is both a property of a programming language and software written
in the language. 

A type safe programming language makes it impossible to misinterpret values of 
one type as another. Most languages try to make it difficult to do that, but 
not impossible. Although this class doesn't cover type systems in depth, we
will talk about common notions of subtyping and value conversion in C++. These
ideas appear in most other languages as well.

Type safe software uses the language's type system to create new data types
to that are difficult to misuse. The goal in designing these data types is to
use the language's type rules to enforce safe usage of those abstractions.
User-defined types, notably enums and classes, can be used to improve
type safety in programs. A primary focus of this course is for you to learn
to use these language features to build type safe abstractions.

# Enumerations

Many properties of a concrete entity (e.g., a playing card) are best described
by a list of values. For example a card has both a rank and a suit. Both rank
and suit can be described by their name, but it's not otherwise clear how you
might represent them within a program. A typical solution is to assign an
integer value to each value of rank or suit (e.g., ace = 1, two = 2, etc.).

These kinds of properties are sometimes referred to as "nominal" values,
especially in statistics. The term "nominal" relates to the importance of the
value's name, not its corresponding value.

However, simply using `int`s to store these values leads to problems. See the
discussion in notes-1.md.

We should be creating a new data type to enumerate these small sets of values.
In particular, we can create an `enum`. For example:

    enum Rank {
      Ace, Two, Three, ..., Jack, Queen King,
    };

    enum Suit {
      Hearts, Diamonds, Clubs, Spades
    };

These declarations define two new enumeration data types: `Rank` and `Suit`. 
The values of these types are the names, called *enumerators*, contained within 
the braces.

Enumeration data types are essentially restricted forms of integer data types.
Each enumerator is automatically assigned a corresponding integer value, 
starting (by default) at 0 and increasing with each subsequent value. 

Because `enum`s are like integers, we can generally use them just like integer 
values. For example, we can do the following:

    Rank r1 = Ace; // Declare variables
    std::cout << (r1 == King) << '\n'; // Compare values
    std::cout << (Ace < Two) << '\n'; // Order values

However, we can't do everything that an `int` can do.

    std::cout << Ace + King << '\n'; // ???

# Conversions

A conversion is a function that transforms a value of one type (called the 
source value and type) into a value of a different type (called the destination 
type). Conversions are a major source of type error because the conversion may 
lose information. This happens when the destination type may not be able to
represent all of the values of the source type.

Recall that a type describes a set of values. For example the C++ type 
`short int` typically represents values in the range `-32,768` to `32,767` 
(or `2^15` to `2^15 - 1`) The type `int` typically represents values in the 
range `-2,147,483,648` to `2,147,483,647` (or `2^31` to `2^31 - 1`).
The set of values of `short int` is a proper subset of the values of `int`.
That is, values of `short int` are entirely contained within the value of
`int`. When thinking about conversions, we generally want to consider this 
subset relation: which values are subset of the the other.

A narrowing conversion converts values of a larger set to the values of a smaller
set (e.g., converting `int` to `short int`). Narrowing conversions are *lossy*;
they can lose information. A more precise way of saying that is to say that
not every value in the source type can be converted a corresponding value in
the destination type. For example, the `int` value `1,000,000` cannot be 
represented by a `short int`. It's up the programmer to determine what should
be done with such values: discard them, give an error, fail immediately, etc.
However, it is generally considered deeply suspect to allow such conversions to 
succeed since it is almost certainly a bug in the program.

A widening conversion converts values of a smaller set to the values of a 
smaller set (e.g., converting `short int` to `int`). Widening conversions do
not lose information, since every value in the source type has a corresponding
value in the destination type. In this sense, widening conversions are *free*,
in the sense that they can be applied without first checking that the source 
value can actually be converted.

To put this in a slightly more formal setting... A widening conversion is a 
*injective functions*. Each value of the source type is mapped to a 
corresponding value of the destination type. A narrowing conversion is a 
*partial function*. Not every source value has a corresponding destination
value.

If we think about types as sets of values, there are two other interesting
cases to consider: the case where the sets are disjoint (or non-overlapping)
and the case where they intersect, but are not subsets. In the disjoint case,
these functions may map values between completely different representations of 
the same value completely different values altogether. In the overlapping case,
the source and destination type share some values in common, but not all.
I would not characterize these as conversions, per se, but simply describe
them as general purpose transformations. Transformations are almost always
implemented by stand-alone functions. For example, `std::stoi` transforms
`std::string` values to `int` values.

The concepts of narrowing and widening conversions are reflected in many
programming languages, specifically whether they must be invoked explicitly
or they can be invoked implicitly.

### Explicit conversions

An explicit conversion is a conversion must be requested by the programmer.
Narrowing conversions are almost always required to be explicit. Explicit 
conversion are a way of acknowledge that the source value is known to 
representable in the destination type. That is, the programmer is assuming 
responsibility for a potentially lossy conversion, and is announcing that
they are guaranteeing a valid conversion. C++ has lots of ways to write 
explicit conversions:

- `static_cast<D>(s)` converts the source value `s` to the destination type `D`
  without a runtime check for validity (hence `static`). In other words, the
  programmer is responsible for determining the correctly of the operation.
  `static_cast` only works with built-in types (e.g., `int`, `double`), and 
  pointers and references to classes. If the compiler can detect that you are 
  using a `static_cast` incorrectly, it will tell you so.
- `dynamic_cast<D>(s)` converts the source value `s` to the destination type `D`,
  and applies a runtime check to ensure the conversion succeeds (hence `dynamic`).
  `dynamic_cast` only works with pointers and references to polymorphic types.
  More on this later.
- `reinterpret_cast<D>(s)` converts the source value `s` to the destination type
  `D`. This is a form of static conversion, with some restrictions. It only
  changes the type of the value, it does not modify the bits. This is often
  used convert pointers from one type to another, which is extremely unsafe.
  Use this with extreme caution.
- `const_cast<D>(s)` converts the source value `s` to the destination type `D`
   where the only difference between the type of `s` and `D` is the presence
   or absence of a `const` qualifier. This is a form of static conversion.
   In other words, you can use to make a `const` object, non-constant. This 
   can also be extremely unsafe, since removing `const` can lead to memory 
   access violations. Use with extreme functions.
- `(D)s` is a C-style cast; it tries to select one or more or static conversions
  in order to convert `s` to type `D`. Use with caution.
- `D(s)` is a functional-style cast, and it's the same as writing `(D)s`.

That's a lot of ways to convert values.

### Implicit conversions

An implicit conversion is one that the programming language applies in certain
contexts to perform a widening conversion. The language's rules for implicit
conversions are somewhat complex, but generally follow the principle that
a narrow type can be converted to a wider type. We'll talk more about this
principle later when we talk about class inheritance.

Implicit conversions are applied during when an object is initialized. Using 
the convention where `D` is a destination type, `d` a destination object and 
`s` is a source value, implicit conversions occur when you write this:

    D d = s;

If `D` can represent the value `s`, then this program will be correct (i.e.,
you don't end up with garbage in `d`). Implicit conversions are also applied
when you call functions:

    void f(D d) 
    {
        // Do stuff with d
    }

    int main()
    {
        S s; // Create an s object
        f(s); // Call f, implicitly converting s to D
    }

Note that C++ considers argument passing to be a form of initialization, so
this really says the same thing as the code above. This is also true for
return values:

    D g() {
        S s; // Create an s object
        return s; // Returns s, implicitly converted to D
    }

### User-defined conversions

NOTE: Read about classes first.

The C++ programming language also lets you design classes to support implicit
conversions between different types. These are called user-defined conversions.
If you are designing the destination class `D`, then you can add a constructor 
that takes a source type object `S`.

    struct D
    {
        D(S s)
        {
            // Initialize this object so that it corresponds to s.
        }
    };

This is quite common. Note the difference between a constructor that implicitly
converts values and a constructor that initializes with a value is pretty thin
at this point. In general, a constructor that takes a single argument is said
to be a *converting constructor*. These get some special treatment in the C++
language.

If you are designing the source class `S`, then you can add an implicit 
conversion to `D`.

    struct S
    {
        operator D() const
        {
            // Make a value of D that corresponds to *this.
        }
    };

This is called a conversion operator. It can be called whenever an `S` object
is requested to be converted to a `D` value. This is not so common to do, but 
if you really want implicit conversions and cannot add a constructor to `D` 
(because you didn't write the class), do this.

As a few final notes on conversions:

- If `S` is implicitly convertible to `D`, then it is explicitly convertible
  using `static_cast`.
- If you have the opportunity to design both `S` and `D` classes, do not add
  both a converting constructor to `D` and a conversion operator to `S`. 
  Choose one.
- As a general rule, avoid writing implicit conversions to built-types. They can 
  have surprising effects.
- As a general rule, do not write value transformations as implicit conversions.
  They can often fail and recovery is usually important.

One last observation about conversions... conversion is deeply rooted in the
notion of *value* and *representation*. These ideas are largely antithetical
to traditional object-oriented programming concepts (no values, only objects
with properties and behaviors). Conversions, especially implicit conversions, 
are not well represented by many mainstream object-oriented languages. For 
example Java allows some widening conversions for built-int types (int `int` 
and `double`), but not for user-defined types. C\# actually does support 
implicit conversion operators, but not converting constructors. I haven't been
able to determine when that feature was added to the language, but I assume
that it was fairly recent (within 10 years?).


