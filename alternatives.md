
# Representing alternatives

A standard deck of cards comes with 54 cards: the usual 52 cards of the four
suits and 2 jokers. Unlike other cards, jokers don't have suits; they are
either red or black (or black and white -- they have different colors).

Suppose we wanted to include Jokers into our game of war. How would we
modify our definitions of `Rank`, `Suit`, and `Card` to allow for the
inclusion of Jokers.

We could modify our definition of `Rank` and `Suit` to somehow accommodate
the inclusion of these oddly valued unsuited cards. Obviously, I don't think
highly of this idea; our definitions of `Rank` and `Suit` are good. We should
avoid watering them down with additional non-rank, non-suit information.

Let's try to think about this more abstractly. In this deck, a card is 
*either* a suited card *or* a joker. We should think about how to modify
our card definition so it can represent *both* suited cards *plus* jokers.

To begin with, let's think about these cards as representing fundamentally
different values. We can simply rename our `Card` class to `SuitedCard`,
and add a separate `JokerCard` class. The value of a jojer card is determined
by its color: red or black. By convention, the red joker typically outranks
the black joker. So we have:

    enum Rank { ... };
    
    enum Suit { ... };

    enum Color {
        Black,
        Red,
    };

    class SuitedCard {
        // as before
    };

    class Joker {
        Joker(Color c)
            : color(c)
        { }

        Color get_color() const { return color; }

    private:
        Color color;
    };

Now, we can think more clearly about the "or" part of the problem: a
card is either suited or a joker. As first pass, we could simply consider
a data structure that stores both suited cards and joker cards, along with
a flag that determines which value we're using. Here's my first pass:

    struct Card {
        bool suited;
        SuitedCard sc;
        JokerCard jc;
    };

Whenever the `suited` member variable is true, the value of the `Card` is
the value of `sc`. Otherwise, the value of the card is that of the `jc`.

This does solve our problem, but not really the right way. Abstractly, I
would interpret this data type being a combination of `SuitedCard` and 
`JokerCard`, not one or the other. In fact, this actually requires the
initialization of both `sc` and `jc` so that both values are potentially
valid.

To really solve the problem, we need to use a `union` of `SuitedCard` and
`JokerCard`. I'm going to call this `CardImpl` because it really is an
implementation detail of our eventual `Card` class.

    union CardImpl {
        SuitedCard sc;
        JokerCard jc;
    };

A `union` is structurally like a `struct` or `class`, but with very different
semantics. Where as a `struct` represents "and" data types (a suited
card is a rank *and* a suit), unions represent exclusive "or" data types (a 
card is either suited or a joker. A `struct` contains storage for all of
its member variables. It has to; all members contribute to the representation
of the value. A `union` contains only enough storage for the largest of its
members. This is because it can store only a `SuitedCard` value or only
a `JokerCard` value (not both).

Unions work a bit like classes too. Here are some things that we can do with
unions.

    CardImpl c; // default construct `c`

It may not be obvious what the default value is. When no value is explicitly
selected, C++ simply selects the first member. In other words, a default
`CardImpl` is a default `SuitedCard`, although we opted to make that trivially
default constructible. In other words, the default value is indeterminate.

You can directly assign to members of `union` to initialize them.

    c.sc = SuitedCard {Ace, Spades};

Assigning to a specific member causes that member to become the *active
member* of the union.

Theory note: In type theory, and often in advanced mathematics, these are
called *product types* and *sum types*. TODO: Finish writing this.