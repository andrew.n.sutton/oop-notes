
# Representing abstract concepts

We need to consider what it is we want model inside a computer. We can generally
think about problems in a few broad classes of things:

- entities
- functions
- systems

## Functions

Entities can be either abstract or concrete. An abstract entity is something
like the number 3. There's no physical instance of the entity. we can write
it down lots of ways: 3, three, III, 2 + 1, etc. However, we all agree that
each of those spellings corresponds to the abstract entity that we know as
3.

A concrete entity is something in the physical or real world: an object, a 
person, a state. Concrete entities are typically described by their abstract
entities. A person has height and weight: both numbers. These attributes
may change over time: a person can gain or lose weight.

TOOD: Write more about abstract and concrete entities. Base that on descriptions
in EoP.

We tend to represent abstract and concrete entities using classes. We group
together the attributes and behaviors needed to build a data type whose
state corresponds to an entity.

## Functions

A function maps inputs to outputs.

We generally implement functions using functions (surprise!), but this is
hardly the end of the story. Many of the functions we choose to write have
associated state. That state could change with each invocation of a function,
or it could provide a constant for use throughout.

TODO: This is a big topic. Explain the basic kinds of functions and link
to sections that describe them.

## Systems

A system is a kind of problem element that resists characterization as either
an entity or function, although they tend to be more function-like than
entity like. The key idea is that a system performs some sequence of actions.
There may or may not be an output, but certain properties or quantities of a 
system may be observable.

An example of a system is a virtual machine that executes a program. There
may not be a concrete output of the machine. However, you can observe a)
the program, b) its memory, and c) it's configuration.



