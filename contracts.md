
## The logic of programming

TODO: This topic would best be given BEFORE we talk about class design.
Students should be taught these concepts incrementally in CS1 and CS2.

For the purpose of this section, assume the following data structures:

    using Deck = std::vector<Card>;
    using Hand = std::vector<Card>;

Consider a simple function that deals one card from a deck to a player's
hand.

    // Deals one card from `d` into `h`.
    void deal_one(Deck& d, Hand& h)
    {
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

We want to know if this is a *safe* function? That is, when we call it,
is it *guaranteed* to deal one card from `d` into the hand `h`.

In order to answer that question, we need to think about the kinds or sets of 
values that can be provided as arguments. To do that, we need to think about
the values of decks and hands. Fortunately, both types describe the same
sets of values: sequences of cards. There are only a few special values
of interest here in this algorithm: when the deck is empty (i.e., the empty
card sequence) and when the hand is full (what is a "full" sequence?).

When the deck is empty, we can't access the last card. There are no cards!
We certainly can't remove the last card. This seems like a relatively easy
issue to fix.

In many card games, a hand has a strict upper bound. For example, in Texas
Hold'em, a player hand is comprised of exactly three cards. Clearly, we
should not be able to deal additional cards into a hand that is already full.

In this case, however, the notion of a "full" `vector` is somewhat vague. It
turns out that there is an upper bound on what a `vector` can hold, but it's
typically so large, that filling it completely will a) either exhaust your
available memory or b) cause your computer to start "thrashing". The outcome
depends entirely on the system you're running the program on. That said,
this is still a concern, but it's one we can put off until later.

If either of these cases are true (the deck is empty or the hand is full), then
the algorithm cannot guarantee its results. In fact, if the deck is empty,
calling this function will almost certainly crash. Try it!

Every function has a *contract*. This is a formal specification of what is
expected of its inputs, and what is guaranteed as a result. The word "formal" 
and "specification" are important in contracts. "Formal" means either checkable
(as in code) or provable (as in math). A "specification" is a description of
*what* values are computed or actions performed, not *how* those computations
or actions are executed. A specification is most definitely not an 
implementation.

Contracts typically have two major components:

- *Preconditions* specify what must be true for function arguments
- *Postconditions* specify what must be true of the function result and other effects

The fact that these are *conditions* means that they may---and often are---checkable
properties. That is, we can execute some C++ expression to check that e.g., arguments
satisfy the precondition (i.e., the expression must be `true`). It can be harder
to define functions to programmatically check postconditions. More on that
later.

The contract for our `deal_one` function might be:

- precondition: `d` shall not be empty and `h` shall not be full.
- postcondition: The back card is removed from `d` and inserted at the back
                 of `h`. Otherwise, no other card values are changed.

Preconditions can also include references to non-local state; this will 
certainly true when we start talking about member functions.
Postconditions usually cover two things: a definition of what
is returned, and a list of side effects. Side effects include modifications
of non-local (i.e., global) variables, modifications to arguments passed by
reference, and data written to files. 

In general, preconditions are easier to define than postconditions---not always,
but usually. The reason is that preconditions are applied to specific values.
Postconditions often need to explain how a value has changed. That turns out
to be computationally difficult. More on that below.

If a function is called with invalid arguments (arguments that do not satisfy 
preconditions), the function is said to be called *out of contract*. In that
case, there is no possible way for the function's definition to guarantee its
postconditions. The result is *undefined behavior*.

This is almost exactly the same as dividing an integer value by 0. This is a 
kind mathematical precondition. You can certainly write $n/0 either on paper or 
in code, but the expression does not correspond to an actual value.
Computationally, the expression does not yield a value, and so any program
relying on the result, cannot continue.

NOTE: You *can* divide floating point values by 0. These are defined to return
either positive or negative infinity.

Consider what happens if we call `deal_one` out of contract.

    Deck d;
    Hand h;
    deal_one(d, h); // d is empty, out of contract

Because `d` is empty, the function `deal_one` could do just about anything.
Remember that the precondition only says when the function must do something.
In the case of our implementation, the program will almost certainly crash.

Pretend, however, that the function returns. After the call is made, what
can you say about the value of `d` and that of `h`? Does `d` have one fewer
cards than before the call was made? Does `h` have one more? We certainly
expect both to be true. After all, that's what the contract claims.

However, the contract isn't valid any more because we violated its terms when
we called the function. At this point, we know absolutely nothing about the
value of `d` or `h`. We can't even trust that they are valid objects any more.
In other words, this program is irredeemably broken. This is a common form of
*logic error*: an error resulting from a programmer calling a function out
of contract.

The following sections discuss issues related to the specification and checking
of preconditions and postconditions.

## Preconditions

Ignoring the case of bounded hands size and memory capacity (for now), the
preconditions to our function are pretty simple: the deck shall not be empty.
We can modify our definition to reflect that, by simply adding a comment to
the function.

    // Deals one card from `d` into `h`.
    //
    // Expects: `d` shall not be empty.
    void deal_one(Deck& d, Hand& h)
    {
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

The precondition is expressed as a comment. It's reasonable in some cases.
Note, however, that the precondition can be easily specified as a C++ 
expression. This would be a bit better.

    // Deals one card from `d` into `h`.
    //
    // Expects: `!d.empty()`.
    void deal_one(Deck& d, Hand& h)
    {
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

Unfortunately, we can't do much with comments. What we'd really like is to
provide some way of giving feedback to callers who violate the contract. The
easiest way to do this is by `assert`ing the precondition.

    #include <cassert> // include this for assert

    // Deals one card from `d` into `h`.
    void deal_one(Deck& d, Hand& h)
    {
        assert(!d.empty()); // Checks the precondition
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

The `assert` function (actually a macro) evaluates the given condition. If the
condition is `false` the program *aborts*. That is, it stops running, but does
so in a way that is "friendly" to debugging. This allows the caller of the
function to better understand their own logical errors.

Assertions are only enabled in certain program build configurations, usually
whenever debugging is enabled. Because release builds typically favor 
performance over safety, assertions are removed in production versions of
the software. They are not checked because the cost of executing the check
can be can be quite high. This means that any code that calls the
function out of contract in release mode can have completely unexpected
results.

This does not mean that assertions are not useful. They're very useful for
ensuring that certain parts of your program contain no logic errors. However,
assertions are not sufficient for all kinds of error checking and error 
handling.

Here is another way we could check the preconditions: throw an exception:

    #include <stdexcept> // includes exception classes

    // Deals one card from `d` into `h`.
    void deal_one(Deck& d, Hand& h)
    {
        if (d.empty())
            throw std::logical_error("empty deck");
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

In this version, we always check that `d` is not empty before performing
the action. By throwing an exception, we allow the program to catch the error
and perform some other action.

This changes the contract. We have defined the behavior of the function in
the case where the deck is empty. Our contract might now look like this:

    // Deals one card from `d` into `h`.
    //
    // Contract: If `d.empty()` is true, throws a `std::logic_error`.
    void deal_one(Deck& d, Hand& h) ...

To look at this in a slightly different way, our function now accepts all
values of `Deck` and `Hand`, and we know what the behavior is in every case,
even though at least one value results in an exception. That's still a defined
behavior.

In other words, we've made the contract *wider*. Our assertion-based 
implementation had a *narrow contract*. A narrow contract accepts only 
arguments that produce valid results. In this case, we deal a card to a
player. Invalid inputs result in undefined behavior. A *wide* contract allows
invalid inputs by has a defined behavior in those cases, like throwing an
exception.

Note that wide contracts are necessarily less efficient. They require an
implementation to identify invalid arguments and act on them. The function
above checks that the deck is empty every time it's called. 

This leads to a design question. When should you prefer narrow contracts
vs. wide contracts? Fortunately, their is a relatively straightforward
answer (one of few): design wide contracts when the inputs are untrusted,
otherwise, prefer narrow contracts. Untrusted inputs almost always conform
to data entered by a user of the system, read from a file, or read from
the network. Programs that work with untrusted or foreign data must ensure
that invalid inputs are caught and handled and not acted upon.

Most software security issues arise from failing to handle ill-formed user
input. This includes network intrusion attacks, malicious email attachments,
SQL injection attacks, etc.

That said, this function is not one of those cases. Dealing cards does not
depend on foreign data, so a narrow contract is an appropriate design. That
will also make the program faster in release mode.

TODO: This really belongs in a separate section on error handling... maybe.
The moral of the story is that exceptions, expected values, and error codes
move the burden of checking preconditions ahead of time to checking 
postconditions after the fact.

That said, let's look at a few other ways of doing this:

We could return an error code:

    // Deals one card from `d` into `h`.
    //
    // Returns `true` if a card is dealt and `false` otherwise.
    void deal_one(Deck& d, Hand& h) ...


Never do this:

    void deal_one(Deck& d, Hand& h)
    {
        if (d.empty()) {
            std::cerr << "empty deck\n";
            return;
        }
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

Standard error is for *logging* errors, not preventing them. In fact, this
makes the program fail silently. It's entirely possible to run this program
in a way that errors are never seen nor acted on. In fact, the program will
likely run to completion although the results will be completely unexpected.


### Details: `assert` and `abort`

`assert` is not a normal function; it is is *preprocessor macro* that 
expands into a small fragment of code that evaluates the condition. If the
condition is false, the program *aborts*, which causes the process to end.

The behavior `assert` macro changes when compiling in release mode; it 
removes the condition from the final compilation, meaning that it is not
checked and will not abort. Failing a precondition check in release mode
results in undefined behavior---you have no idea what the result of calling
the function will be.

Here is a possible definition of `assert`.

    #ifdef NDEBUG
    #define assert(E)
    #else
    #define assert(E) if (!(E)) std::abort();
    #endif

The definition is predicated on the preprocessor macro `NDEBUG`, which
when defined, implies that no debugging information should be emitted. In
other words, compiling in release mode sets `NDEBUG`.

The `abort` function causes the program to abort. Aborting a process means
it ends, and in this case, no cleanup is performed: destructors of objects are 
not executed, nor are other program cleanup routines called. The reason
that clean is not performed is that aborting a program typically implies that
no code can be safely executed due to a logical error. Doing so may result
in undefined behavior.

## Postconditions

Postconditions are the guarantees made by the function as part of its contract.
There are two kinds of postconditions: those that describe the return value
and those that define side effects.

Side effects describe any modifications to the program state other than the
returned value. These could (and frequently do) include the modification of
function arguments. They could also describe meaningful changes to global
variables or member variables of a class.

Defining postconditions can be difficult. For example, how do you specify that 
the square root function `sqrt` returns the square root of its argument? 
Typically we appeal to mathematics: `sqrt` returns $\sqrt{n}$. What we must
not do is write the specification as an implementation; the two are distinct.

NOTE: It is not a recursive definition to say that `sqrt(n)` computes
$\sqrt{n}$. The latter is a specification, and the former refers to its
implementation.

NOTE: Formal methods is an approach to software engineering where function
definitions are proven (mathematically) to satisfy their postconditions.
However, formal methods are often eschewed by developers because of their
up-front cost: time spent proving the correctness of an algorithm is time
not spent developing new features or fixing old ones. 

In many cases, postconditions are defined in relation to the original value
of a function argument. Consider the shuffle function for a function that
deals a single card... How might we formally specify the postcondition?

    // Deals one card from `d` into `h`.
    void deal_one(Deck& d, Hand& h)
    {
        Card c = d.back();
        d.pop_back();
        h.push_back(c);
    }

Think about the effects on the `d` and `h`. Well, it shrinks by one, and
`h` grows by one. Is that sufficient? Probably not, because we haven't really
said how `d` shrinks and how `h` grows. We need to specify: the last card
is removed from `d` and appended to `h`. That's not too bad, but it's pretty
close to saying what the implementation does.

We could be more precise by defining the effects in terms of abstract values:

> If `d` has the value $(d_1, d_2, ..., d_{m-1}, d_m)$ and
> `h` has the value $(h_1, h_2, ..., h_n)$, then after completion, 
> `d` has the value $(d_1, d_2, ..., d_{m-1})$ and 
> `h` has the value $(h_1, h_2, ..., h_n, d_m)$.

This defines the effects of the functions in a purely mathematical way. It
also clearly shows the relationship between the original values of `d` and
`h` and their values after executing.

Testing postconditions that refer to the original value can be difficult.
For example, asserting this postcondition would require that we capture the
original values so that they can be compared to their modified values. If
we really wanted to, we could define this function in terms of some helper
functions:

    void deal_one(Deck& d1, Hand& h1)
    {
        auto [d2, c] = take_from_back(d1);
        Hand h2  = add_to_back(h1, c);

        // d2 has the value (d1, d2, ..., dm-1)
        assert(d2.size() == d1.size() - 1);
        assert(std::equal(d2.begin(), d2.end(), d1.begin()));

        // h2 has the value (h1, h2, ..., hn) and ...
        assert(h2.size() == h1.size() + 1);
        assert(std::equal(h1.begin(), h1.end(), h2.end());
        
        // ... the last card in the hand is the one removed.
        assert(h2.back() == d1.back())
        
        d1 = d2;
        h1 = h2;
    }

However, this checking adds a lot of overhead: two more allocations and
copies!. It also complicates the algorithm significantly. There are
other ways to check postconditions, but they tend to be even more intrusive.
For example, we could rely on the preprocessor to conditionally remove the
postcondition checks the same way that `asssert` removes those checks.

As a result of the added complexity to an implementation, postconditions are 
checked infrequently in practice.

## Contracts and classes

Member functions have contracts too. However, these contracts are defined
with respect to the entire object, and not just the arguments to the functions.
In this case, member variables act kind of like global variables; they provide
an additional, implicit source of requirements for preconditions and 
postconditions.

Before diving into a broader discussion on specifying contracts on member
functions in general, we need to consider special members functions: namely,
constructors and destructors. Recall that a constructor is invoked to create
an object in memory (it does not create the memory), and that a destructor
is invoked to "tear down" the object in order to release resources.

The most essential postcondition of a constructor is that it produces
an object that corresponds to an abstract or concrete value. This means that
any arguments to the constructor must be used to produce a valid value.

Describe preconditions in constructors.

NOTE: There are cases where this isn't quite true. For example, our default
constructor for this class is this:

    class Card {
        Rank rank;
        Suit suit;
    public:
        Card() = default;    
        ...
    };

This constructor will produce cards whose rank and suit are indeterminate 
values. This is an exceptional case, and users of the class should be clearly
warned that a valid value must be assigned to the object prior to its use.

Destructors have no preconditions, and their postcondition is that be that any
resources acquired during the lifetime of the object are released. 

Handling errors in destructors must be done with care. It is not permitted,
for example, to allow an exception to propagate through a destructor.

## Class invariants

Often, as part of guaranteeing the construction of a valid object, certain
relationships must be established between the data members of the 
class---or even the bits in a single data member.

Let's consider an optimized representation of our playing card values. We
need only 2 bits to encode the suit of a class and 4 bits to encode the rank.
We can compact these into an 8-bit value of the form:

    0b00ssrrrr

Here `ss` denotes to the bits that encode the suit value and `rrrr` denotes 
the bits that encode the rank value. The 2 high-order bits must be 0.

Now, let's add the ability to represent jokers within the same 8 bits. To do
this, we need only one bit: whether the card is red or black. However, we also
need to indicate when the representation is either suited or a joker. We can
do that by using the high-order bit(s) to select the representation. In other
words, we can make jokers have this form:

    0b1000000c

Here, `c` is the color bit.

Because our representation can store either set of values, our class has moral
obligation to ensure that only one form is active at a given time. This is
called a *class invariant*: a property (or set of properties) about the object
that is true for the lifetime of the object. In this case, the class invariant 
is that the representation matches one form or the other. Any other pattern
of bits does not correspond to a card value.

Every other member function of the class *assumes* that these invariants
are value. Their implementations rely on these properties in order to provide
correct implementation. If an object's invariants are somehow broken, then
the class' behavior will be undefined.

As noted above, the purpose of a constructor is to produce a valid object.
It does this by establishing class invariants. 

NOTE: The invariants of previous card class example are trivial; there is no 
inherent relationship between the `rank` and `suit` members that needs to be 
enforced. That's why we moved to the more advanced option.

Here is the partial definition of our new, compact `Card` class.

    class Card {
        unsigned char bits;
    public:
        Card(Rank r, Suit s)
            : bits((s << 4) | r)
        { }

        Card(Color c)
            : bits(0x80 | c)
        { }

        bool is_joker() const { 
            return bits & 0x80;
        }
        
        bool is_suited() const { 
            return !is_joker(); 
        }
    };

Each constructor builds exactly one binary representation of a value. The
first constructor builds a suited representation in which the two high order 
bits are zero, and the second builds a joker representation in which the high 
order bit is set and the low order bit is either black or red.

We rely on this invariant (the alternative representation) in order to determine
whether a card is a joker or not.

SIDE NOTE: This representation actually optimizes comparisons too. The binary
representation gives jokers the highest value and partitions the suited
values into contiguous classes corresponding to their suit. Now, comparisons 
are simple integer operations. What does this have to do with invariants?
Nothing at all.

We also rely on this invariant to define additional preconditions on accessor
functions for the suit and rank:

    class Card {
    public:
        ...
        Suit get_suit() const {
            assert(is_suited());
            return (Rank)(bits & 0x30) >> 4;
        }
        Rank get_rank() const {
            assert(is_suited());
            return (Suit)(bits & 0x0f);
        }
    };

TODO: Explain this.

Furthermore, we rely on that invariant to supply the color of any card:

    class Card {
    public:
        ...
        Color get_color() const {
            if (is_joker())
                return (Color)(bits & 0x01);
            else
                return (Color)(get_suit() >= Hearts);
        }
    };

For joker cards, the color is encoded explicitly. For suited cards, the color
is determined by the suit. (The implementation relies on the fact that boolean
values can be converted to integers and then into a valid color representation:
0 for `Black`, 1 for `Red`.)

This may all seem obvious, but without a precise specification of the internal
state of an object, we would not able to define these operations.

TODO: Describe how invariants are required to be re-established by

## Computational limitations

Do you remember when we put off concerns about memory allocation? Maybe we
should re-consider those.

Examples of errors derived from computational limitations:

- memory allocations
- arithmetic overflow
- floating point error


