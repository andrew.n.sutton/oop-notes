
# Classes

Classes are the primary abstraction mechanism in all object-oriented programming
languages. Classes are used to create new aggregate data types. 
(Aggregate roughly "composed of other data types").

The goal is to defined data types that *represent* (or encode) abstract or
concrete entities (i.e., values).

TODO: Do a better job defining these terms.

- value -- an abstract or concrete entity
- representation -- the data types used to implement 
- object -- memory that holds a value
- state -- the values of an object's representation
- interpretation -- the value corresponding to an object's state

TODO: This section focuses on defining value types. A *value type* represents
an abstract or concrete entity. A value type implementation may require dynamic 
memory. These are interesting classes in their own right, and separate from
the data type we'll be talking about here.

NOTE: Classes are also used to represent functions and systems. 



Our first attempt at defining a `Card` class looks like this:

    struct Card
    {
        Rank rank;
        Suit suit;
    };

A `struct` is a class; we could also have used the keyword `class`, but that's
for later.

This definition allows anybody to modify the member data of the class. This is 
because the members of the class are *publicly accessible* (members of a
`struct` are public by default). By making the members public, we haven't
created any strong guarantees that other programmers should not modify those
values. This could potentially lead to unintentional modification of a card's
state, resulting in bugs later on.

## Access specifiers

All modern object-oriented languages provide a mechanism that allows you to
control to *access* to the members of a class. A member of a class is 
accessible if it can be used. We generally characterize the notion of "use" 
based on who is doing the actual using. This tends to be one of two groups of 
programmers: the person writing the class, and the person using the class.

Most literature on object-oriented programming will tell you that the member
variables of a class are part of the class' *implementation details*. In the 
case of our playing card class, this is how we represent or encode the value of 
a playing card. Implementation details are only relevant to the person 
implementing the class, and must either inaccessible or invisible to a person
using the class.

For example, the implementation of `std::string` is very complicated these
days --- far more complex than a `char*` and its length. We, as users of that
class cannot access the implementation details because a) we might not 
understand them, and b) we would be tempted to use them in a way that leads
to buggy code. Said in a slightly different way, the implementer of 
`std::string` is trying to protect us from ourselves.

We'd like to do something similar for our `Card` class in order to prevent
clever game writers from manipulating our data structure in a way that we
don't want. Specifically, we want to make our data members *private*.
The simplest way to do that is to make it a `class`.

    class Card
    {
        Rank rank;
        Suit suit;
    };

The only difference between a `class` and a `struct` is default access. For
`struct`, the default is `public`. For `class`, it's `private`.  If you try to
access these members, even through initialization, your program will not
compile:

    Card c1 {Ace, Spades}; // error: rank, suit are private

    Card c2;
    c2.rank = Ace; // error: rank is private

So, in our effort to restrict access to the "implementation details" of our
class, we've made it unusable. To make the class useful again, we're going
to have to write more code.

Herein lies an interesting problem. We'd like our code to be as simple and
obvious as possible. We'd also like to make it as safe as possible -- to prevent
as many user errors as we can. As soon as we commit to making our abstractions
safer by making implementation details private, we also commit to making our
classes more complex. In this case, we're going to have to provide: constructors
and accessors that make the class usable.

### Style note

In practice, you'll often see classes written like this:

    class SomeClass
    {
    public:
        // Public members here

    private:
        // Private members here 
    };

It doesn't matter whether you put the private members first or last, or if
you mix them up. You could write your class like this:

    class SomeClass
    {
        // Private members here 
    public:
        // Public members here
    };

It's all the same to the compiler, so choose a style that you like. Preferably.
one that makes it obvious what's private and what's public.


Note: When we talk about the *visibility* of a name, we're referring to our
ability to find it in scope (by lookup). When we talk about *accessibility*,
we're referring to whether we have permission to use the name. Both of these
concepts involve *information hiding*; the idea that we want to hide nitty
gritty details of an implementation or representation from the people using
our classes, functions, and modules.


## Constructors

We'll start by figuring out how to initialize a card object. This makes sense;
you can't use an object that you can't create. In order to figure out what 
operations we need to provide, let's think about the ways we might create the
object.

Certainly, we want to be able to write this:

    Card c {Ace, Spades};

We expect this to make a card with the Ace of Spades value. 

Because we've made our data members private, we're going to have to write a
special function called *constructor* to initialize `c` with those values.
Of course, the constructor must be public because we're using it in e.g.,
`main`. Here's what that looks like:

    class Card
    {
        Rank rank;
        Suit suit;
    public:
        Card(Rank r, Suit s)
            : rank{r}, suit{s}
        { }
    };

The constructor is a function with no return type and the same name as the
enclosing class. In this case, our constructor takes two arguments: the
rank and suit.

The `: rank{r}, suit{s}` notation is called a member initializer list. This
gives you (the implementer of the constructor) the opportunity to explicitly
initialize the values of the members `rank` and `suit`. Essentially, we are
calling constructors for those objects.

FIXME: Write extensively about establishing invariants. Note that the invariants
in this class are maintained by the type system. You can't (easily) create
invalid `Rank`s and `Suit`s.

### Style note

Prefer to use the member initializer list to initialize members of a class.
Don't assign values in the body.

For various reasons, this isn't a concern in languages like Java or C#, but
in C++, those assignments can be redundant and can make your programs a bit
slower.

The reason for this is that C++ will implicitly call the default constructor
for every member variable not explicitly mentioned in the member initializer
list. This guarantees that the member variable is valid within the body of
the constructor. An assignment within the body overwrites the default value.

Suppose a member variable allocates some memory in its default constructor. 
The assignment in the constructor's body could cause that memory to be deleted
and reallocated, even though the previous memory was never used. This is not
only a redundant initialization, it's wasted time.

### Style note

Constructors can also be called using parentheses. 

    Card c(Queen, Hearts);

Should you use parenthesis to explicitly invoke a constructor, or should you
use braces? I would say that it depends on the arguments to initialization. 
If the arguments fully specify the value of the class, then use braces. 
If the arguments specify an initialization strategy, use parentheses.

This is more obvious with, say, `std::vector`. Here are some examples:

    // Creates a vector whose value is the sequence 1, 2, 3.
    // Use braces because you're specifying the value.
    std::vector<int> v {1, 2, 3};

    // Creates a vector with 4 default integers. The constructor
    // argument says how to construct the vector, not explicitly 
    // what the value is.
    std::vector<int> v(4);

This is purely my opinion, but I like this style.

## Default constructors

Should we be able to write this?

    Card c;

In other words, should the `Card` class be default constructible? It's not
very likely that our programs would ever have a single uninitialized `Card`
variable, but we might have arrays of them. For example, the spoils in the
game of war may be 3 cards. If we want to write this,

    Card spoils[3];

... then we're going to need a default constructor.

Depending on how we initialize our deck of cards, we might also want to
write this:

    std::vector<Card> deck(52);

This would create a sequence of 52 default-initialized playing cards. Again,
we're going to need to write a default constructor.

NOTE: The following is a long discussion of default constructors, default
values, and language semantics. If you want the TL;DR, search for it.

The purpose of a default constructor is to assign a default value to an object.
What makes a good default playing card? A common answer seems to be the
Ace of Spades. We could write our default constructor thusly:

    class Card
    {
        Rank rank;
        Suit suit;
    public:
        Card()
            : rank{Ace}, suit{Spades}
        { }

        // ...
    };

The default constructor takes no arguments and explicitly initializes the
member variables with pre-specified values.

Why start with the Ace of Spades? Does it have any specific meaning that makes
it better than any other card? Not really. We could choose to simply use the
default values of `Rank` and `Suit`.

    class Card
    {
        Rank rank;
        Suit suit;
    public:
        Card()
            : rank{}, suit{}
        { }

        // ...
    };

Here we've just left out the arguments to the members in the member initializer
list. This conceptually invokes the default constructor for those objects.
For all built-in integer, floating point, pointers, and `enum`s, this will
be the value 0.

We might want to be a careful about doing this, however. It's possible that
we've just created an object that doesn't correspond to a playing card. What
happens if we defined `Rank` like this?

    enum Rank
    {
        Ace = 1, 
        Two, 
        ...
    };

The default constructor would create the card whose representation is the
pair (0, 0). The first 0 doesn't correspond to a valid `Rank`, so the object
isn't a valid playing card. That could be a bit of problem... See the section
on invalid values.

On the other hand, maybe it's not so bad. After all, C++ already has a set of 
types for which default construction does something a bit odd. For example, 
most programmers are typically warned against doing this:

    int x;
    std::cout << x;

The reason often given is that `x` has a garbage value and that's what you see
when it's printed. In fact, C++ makes this undefined behavior, so the program
is technically wrong.

### Language details

Consider the fragment of code.

    int x;
    std::cout << x;

People often say that `x` is not initialized in this case, but that's not
technically true. In reality, C++ says that `x` is trivially default 
initialized. In this context, the word `trivial` essentially means "no code is 
executed". Here, that means that no value is stored in the variable. C++
says that, after trivial default initialization, `x` has *indeterminate value*.

This may seem like an odd way of describing how nothing happens. We do this
for consistency. Remember, C++ requires memory to be initialized before it can
be used as an object. Trivial initialization is simply the no-op version of
turning memory into an object.

The language could have required a default value for these variables. However,
there are a lot of programs that do things like this:


    int array[1 << 16]; // 64K ints
    for (int i = 0; i < (1 << 16); ++i)
        array[i] = some_value();

Requiring a default value in this case would make assignment in the `for` loop
redundant. In other words, trivial default initialization makes programs
faster.

Curiously, indeterminate values are not always unpredictable. If you compile
your software in release mode, then indeterminate values typically have whatever
contents were previously in that region of memory. This might appear to be
random (often large) values. If you're looking at those values in a debugger,
you might actually be able find the value of a previously initialized object.

If you compile in debug mode, modern compilers will often provide concrete
values indeterminate values. Many will simply initialize with 0. I think this
can be a bit misleading because it might make you think that you've actually
initialized a value. Some compilers install *trap values*: specific patterns of 
bits that are interpreted by the operating system as an error indicator. When 
used, they usually cause the program to crash (or trap).
For example, Microsoft's C++ compiler used to "trivially" default initialize 
pointers with the pattern `0xCDCDCDCD`. When used, these would cause the 
program to crash, launching the debugger. I don't know if this is still the
practice.


### Indefinite values

We could do the same thing with our `Card` class: simply make the default
state an indeterminate value. If we can't pick a good default value, but
we still want to create `Card`s without explicitly specifying a value, then
we should consider making the "default" value an indeterminate value.

But, rather than explicitly initializing our
data members, which would make the value definite, we'll just let the compiler
do that for us. Here's a new version of our class.

    class Card
    {
        Rank rank;
        Suit suit;
    public:
        Card() { }

        // ...
    };

This definition simply omits the member initializer list from the default
constructor. The compiler will default initialize the `rank` and `suit` members.
Because these are `enum`s, the compiler will not store a value in those
objects. That is, they are trivially default initialized.

### Defaulted default constructors

But we can still do a little better. Because we've actually defined a function
to initialize the card, it *must* be called. In other words, we've essentially
required a call to a function that does nothing.

What we really want is to make the compiler generate the definition for us, and 
to do so in a way where the compiler can avoid making a function call. We
can do that by making the constructor `default`.

    class Card
    {
        Rank rank;
        Suit suit;
    public:
        Card() = default;

        // ...
    };

The `= default` after the constructor tells the compiler to generate a 
constructor based on the definition of the class. Essentially, the compiler
will try to default initialize every member of the class. For this class,
it gives us something equivalent to the previous version of the class, although
possibly a little bit faster.

This isn't quite the end of the story for default constructors. For very
advanced programming techniques, it can matter whether you implement this
default constructor using an explicit function definition, or simply writing
`= default`. Which form of default constructor is *detectable*. Advanced
programmers can rely on that property to optimize initialization patterns
for certain data structures. Hint: the standard library does this everywhere.
This is broadly called *type-based optimization*, but it's an advanced topic
and this particular application is very specific. This is clearly out of
scope for this class.

Details aside, explicitly defaulting the default constructor turns out to be
quite common because of the rules that C++ uses to generate special member
functions. The first two of those rules are:

- If there are no user-defined constructors, generate all special constructors.
- If there are any user-defined constructors, don't generate the default
  constructor.

In other words, if you write any constructor, but need a default constructor,
you have to write one. In most cases, writing `= default` is the right thing
to do. The exception cases are classes that manage resources.


### Default member initializers

You don't always need to write a constructor. You can choose to initialize
member variables directly with their default values.

    class Card 
    {
        Rank rank = Ace;
        Suit suit = Spades;
    public:
        // ...
    };

When the compiler generates the default constructor (probably because you
used `= default)`, it synthesizes a constructor that looks like this:

    Card()
        : rank(Ace), suit(Spades) 
    { }

In other words, you get exactly what you would have written yourself.


### Invalid values

One option considered during class was to explicitly choose a representation
that did not conform to an actual playing card. We called this the "not a card"
value. Said otherwise, we want the state of our object to encode all valid
playing cards plus a not a card state.

NOTE: The word "plus" in the sentence above is not an accident. This is
clearly a reference to sum types, although that's slightly more advanced topic.

We could do this by adding a new `Rank` or `Suit` enumerator that indicated
invalidity. Or, we could add a boolean member to indicate whether the `rank`
or `suit` were indeed valid. The latter makes more sense to me, since we
wouldn't clutter our well-designed `Rank`s and `Suit`s with "fake" values.
Here is that implementation:

    class Card
    {
        bool valid = false;
        Rank rank;
        Suit suit;
    public:
        Card() = default;
        Card(Rank r, Suit s) ...

        bool is_valid() const { return valid; }
    };

I've used a default member initializer and defaulted constructor to make
default-initialized cards invalid. The `is_valid` member function can be
used to determine if the playing card is actually a card or not. So now, if
we write:

    Card c;
    c.is_valid() // will be true

This might seem like a good idea, but it comes at cost. Because we've 
explicitly made it possible for a `Card` to be invalid (not a card), then we
have to ensure any algorithm, function, or game that evaluates the properties 
of a card explicitly excludes the invalid states. After all, how would one
compare "not a card" with a Two of Clubs in the game of War? Which wins?

To reiterate, this is not a bad design choice, it just imposes some
cognitive and programmatic overhead. That said, if the only reason for adding 
this extra state is to make default construction "work", then it is probably
the wrong choice.

This class actually defines form of *optional value*. An optional value is
one whose normal set of values is extended with an extra state indicating
that the actual value may be missing. This is implemented by the C++ class
template `std::optional`. Optional values are often used when a specific 
operation can fail to return a valid value.

TODO: Link to `std::optional`.


### Default constructors: TL;DR

If there's an obvious default value for your class, either write a constructor 
that initializes your members to that representation or use default member
initializers to specify the value.

If you have to write a default constructor (because you've written some other
constructor), and choosing all defaults is appropriate, then use `= default`.
