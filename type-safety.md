# Types and safety

The type system of a programming language is the set of types it uses to
a) describe values and b) determine when operations on those values are valid.
Every language has a set of built-in types (e.g., `int`, `bool`), and most
have ways to define new types. C++ has three ways of defining new types:
enumerations, classes, and unions.

A *type error* is kind of bug that results from using a value in a way other
than its original intent, typically by misinterpreting its representation.
For example, early languages did not differentiate between integer and floating
point values (bits are bits). In such languages it was fairly easy to 
misinterpret a floating point value as an integer, with seemingly random bits.

Other kinds of type errors are more subtle. For example, certain conversions
may lose information and cause bugs that way. The Ariane 5 crashed in 1996
as a result of converting a 64-bit double to a 16-bit integer, essentially
truncating the value. Automatically or incorrectly narrowing one set of values
to another is a common source of computation bugs.

Most modern approaches to programming involve the application of the type system
to eliminate or reduce type errors. This property is known as *type safety*.
Type safety is both a property of a programming language and software written
in the language. 

A type safe programming language makes it impossible to misinterpret values of 
one type as another. Most languages try to make it difficult to do that, but 
not impossible. Although this class doesn't cover type systems in depth, we
will talk about common notions of subtyping and value conversion in C++. These
ideas appear in most other languages as well.

Type safe software uses the language's type system to create new data types
to that are difficult to misuse. The goal in designing these data types is to
use the language's type rules to enforce safe usage of those abstractions.
User-defined types, notably enums and classes, can be used to improve
type safety in programs. A primary focus of this course is for you to learn
to use these language features to build type safe abstractions.
