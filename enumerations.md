
# Enumerations

Many properties of a concrete entity (e.g., a playing card) are best described
by a list of values. For example a card has both a rank and a suit. Both rank
and suit can be described by their name, but it's not otherwise clear how you
might represent them within a program. A typical solution is to assign an
integer value to each value of rank or suit (e.g., ace = 1, two = 2, etc.).

These kinds of properties are sometimes referred to as "nominal" values,
especially in statistics. The term "nominal" relates to the importance of the
value's name, not its corresponding value.

However, simply using `int`s to store these values leads to problems. See the
discussion in notes-1.md.

We should be creating a new data type to enumerate these small sets of values.
In particular, we can create an `enum`. For example:

    enum Rank {
      Ace, Two, Three, ..., Jack, Queen King,
    };

    enum Suit {
      Hearts, Diamonds, Clubs, Spades
    };

These declarations define two new enumeration data types: `Rank` and `Suit`. 
The values of these types are the names, called *enumerators*, contained within 
the braces.

Enumeration data types are essentially restricted forms of integer data types.
Each enumerator is automatically assigned a corresponding integer value, 
starting (by default) at 0 and increasing with each subsequent value. 

Because `enum`s are like integers, we can generally use them just like integer 
values. For example, we can do the following:

    Rank r1 = Ace; // Declare variables
    std::cout << (r1 == King) << '\n'; // Compare values
    std::cout << (Ace < Two) << '\n'; // Order values

However, we can't do everything that an `int` can do.

    std::cout << Ace + King << '\n'; // ???
