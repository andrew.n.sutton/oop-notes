
## Copying and comparing objects

There are two ways to copy the value of an existing object: declarations
and assignment. Let's start with a card with a fixed value:

    Card c1 {Ace, Spades}; // Declared to be the Ace of Spades
    
If we want a new object with the same value, we can declare a variable to
be a copy of another object.

    Card c2 = c1; // Declared to be a copy of c1

After this declaration variable `c2` should also be the Ace of Spades.

If we have an existing card object and simply want to give it a different value,
we can assign it to be a copy of another card.

    Card c3; // Declared to be indeterminate
    c3 = c1; // Assigned to be a copy of c1

After assignment, `c3` should be the Ace of Spades.

In both cases say the value "should" be the Ace of Spades, but the value really 
*must* be the Ace of Spades. If `c2` is not the Ace of Spades, then your 
`Card` class is morally incorrect since it violates programmers' long-ingrained
expectations about copying. You would be inviting people to misuse your library,
to make mistakes, and to write buggy code.

One last observation. Copies are different objects with their own values.
Modifying one must not result in changes to the original. For example:

      Card c1 = Card {Two, Hearts};
      Card c2 = c1;
      c2 = Card {Three, Clubs};

If, for some reason, the assignment to `c2` modified the value of `c1`, then
your program is morally incorrect. Same as above. Programmers generally expect
the value of each object to be *independent* of any other value or object
in a program. Fortunately, that reflects the real world in a nice way: cards
don't usually change rank or suit while you hold them.

That said, you would have to work pretty hard to make that mistake with the
`Card` class. It's much easier to do with classes that manage resources, 
especially since C++ gives us the tools to do so. We are allowed to define
what it means to copy and assign our data types.

The following two sections show you how to provide user-defined operations for
copyable value types. The section after that shows you how to avoid writing
them.

### Copy constructors

A *copy constructor* provides a way for a programmer to define what it means
to declare a copy of an object. I.e., this case:

    Card c1 {Ace, Spades}; // Declared to be the Ace of Spades
    Card c2 = c1; // Declared to be a copy of c1

The declaration of `c2` invokes a copy constructor to initialize that object
with the value of `c1`. 

Copy constructors are also used when passing objects to functions by value.

    void print(Card c);

    int main() {
        Card c {Ace, Spades};
        print(c); // A copy of c is passed to print().
    }

This makes a lot of sense. If `print` modifies the value of `c`, then it's
just changing the value of its copy. Those changes must not be reflected in
`main`. If they were, this wouldn't be pass-by-value, would it?

We can define that constructor like so:

    class Card {
    public:
        Card(const Card& c)
            : rank{c.rank}, suit{c.suit}
        { }
      
        // ...
    };

A copy constructor always takes a constant reference to an object of its own
type (i.e., `const Card&`). If you forget the reference, your program will
fail to compile. If the type is anything but `Card`, it's not a copy 
constructor.

In this definition, we copy initialize of `rank` and `suit`, providing the 
corresponding values from the original card. There's really nothing special
that we're doing here: just copying the members of the original class. For
classes that manage resources, we would have to do a bit more (i.e., request
resources for our own value).

### Copy assignment operators

A *copy assignment operator* provides a way for a programmer to define what
it means to assign a copy of one object to another. I.e., this case:

    Card c1 {Ace, Spades}; // Declared to be the Ace of Spades
    Card c2; // Declared to be an indeterminate card
    c2 = c1; // c2 is assigned to be a copy of c1

Copy assignment operators are just overloaded operators. Assignment operators
must be defined inside of their class. Here is our copy assignment operator
for the `Card` class:

    class Card {
    public:
        Card& operator=(const Card& c) {
            rank = c.rank;
            suit = c.suit;
            return *this;
        }
    };

A copy assignment operator has this form. It takes a constant reference to
its class and returns a non-constant reference to the same type.

In some cases, it is useful and reasonable to pass the argument by value. 
However, this is not one of those cases. That can be done when your value
type manages resources.

The function simply assigns new values to `rank` and `suit` from their 
corresponding members from `c`. Assignment operators always return `*this`:
a reference to the object being assigned (the right-hand operand of `=`). This 
allows assignment to be chained. For example:

    c1 = (c2 = c3); // Parens are not needed.

Here, `c2` is assigned to the value of `c3`. Because the the assignment
operator returns a reference to `c2`, the next assignment is just this:

    c1 = c2;

This is what we should expect. 

Aside: Advice from various sources may suggest returning something other than a
reference. I've seen recommendations to return by value or even `const`
value. Don't do that. Returning a value can invoke an extra copy. That's cheap
for `Card`, but not for more complex data types.

I've also seen advice to return `void`, but there are legitimate use cases for
making assignment return a value. These typically appear in loops in conjunction
with conversion to `bool`.

    while ((x = f())) {
        // ...
    }

The condition of the loop assigns a new value to `x`. If the result of the
assignment can be converted to `true`, then body of the loop executes.

It is would not be unreasonable to return a constant reference. However, that
would make this statement ill-formed.

    (c1 = c2) = c3;

In that case, we would assign `c2` to `c1` and return a constant reference
to the object `c1`. C++ (rightly) says that you can't assign to or modify
a constant object. 

If assignment returns a reference, as we do above, then this will happily
compile and do what you would expect it to: copy `c2` to `c` and then 
immediately overwrite that value by assigning `c3` to `c1`. Is that useful?
Not likely. I've never seen this code in the wild. But this is consistent
with how assignment works for `int`s. As a general principle, we should try
to make our user-define value types behave just like built-in types. We
want to be as consistent as possible.
This consistency or regularity is actually important. It is a central 
tenet of generic programming in C++.

Historically, this behavior originated in the C programming language. C doesn't 
have references or `const` qualifiers. Instead expressions are classified as 
being either lvalues or rvalues. An *rvalue* is an value of the expression's 
type (e.g., `int` or `float`). An *lvalue* expression is a memory location (of 
an object of some type). Essentially lvalues are a kind of reference.
In C, the expression `x = y` yields an lvalue in ordder to allow chained 
assignment (`x = y = z`). That rule, and absence of `const` qualifiers
makes this work (`(x = y) = z`). Because C++ started out being based on `C`, we 
inherited those rules.


### Implicit copying

As previously noted, our copy constructor and copy assignment operator aren't 
particularly complicated. They perform member-wise initialization and 
assignment respectively. This is a task that the compiler is particularly
good at automating. We could, for example, define those operators to be
defaulted, like we did with the default constructor.

    class Card {
    public:
        Card(const Card& c) = default;
        Card& operator=(const Card& c) = default;

        // ...
    };

By make these functions `= default`, the compiler will supply behavior that
we had previously written by hand. 

That said, C++ is a bit biased towards value types and towards copying. It
turns out that we don't actually have to write anything at all in order to
make our class copyable. Here is a definition of `Card` that is default
constructible, copyable, and constructible over `Rank` and `Suit`.
    
    class Card {
    public:
        Card() = default;
        Card(Rank r, Suit s);

        // ...
    };

That's it. C++ will automatically generate a copy constructor and copy 
assignment operator if you don't write either. These are called the implicit
copy constructor and copy assignment operator.

The full set of rules for implicitly generating data members is quite
complicated and tries to deal with lots of seemingly uncommon edge cases.
In general, if your value type is a set of normal member variables (no
`const` or volatile members, no references, etc), then you can probably skip 
defining the copy constructor and copy assignment operator. A notable exception
is for value types that manage resources. In that case, you almost certainly
need to provide your own interpretation of what it means to copy those values.

### Equality comparison

NOTE: Much of the discussion in this section has deep roots in mathematics
and formal logic. The goal is to explain what equality is, what it means in
C++, and in some cases, how it should be implemented.

The notion of *equality* is inherently related to the notion of copying. In
particular, we have a strong expectation that, if some object `a` of any value
type `T` is a copy of another object `b` (of the same type), then `a` is equal 
to `b`. Or, to write that in C++:

- If a variable `a` is declared as `T a = b`, then `a == b` is `true`.
- If the variable `a` is assigned as `a = b`, then `a == b` is `true`.

These are the language's axioms governing copying and equality. Axioms are
properties that we assume as a basis for reasoning (mathematically) or, in
this case, constructing valid programs. In other words, if you write a class
that can be copied and compared using `==`, then you had better make sure
that those rules above hold for all values represented by your type.

Side note: The actual interpretation of these axioms is that that they specify
a set of values for which these properties hold. Some data types have 
exceptional states for which these axioms do not hold. For example, floating
point types have a NaN encoding to represent undefined values. Trying to use
those values in a context where copy semantics are required will result in
unexpected and therefore undefined behaviors.

We can implement operator `==` by overloading the operator for our `Card`
class. There are a few different ways this can be written; let's start with
a non-member function.

    bool operator==(Card a, Card b) {
        return /* ??? */;
    }

What should this function actually do? What criteria should we compare in 
order to determine `a` is equal to `b`?. In this case the answer is pretty
straightforward: `a` and `b` must have the same rank and suit. That is:

    bool operator==(Card a, Card b) {
        return a.get_rank() == b.get_rank() &&
               a.get_suit() == b.get_suit();
    }

Notice the definition follows the same pattern as the constructors. Instead
of copying each member in turn, we are comparing each member in turn. Compare
the ranks of `a` and `b` and if they're equal, compare their suits.

For value types, the definition of `operator==` is defined in terms of the 
abstract or concrete entities the class represents. Again, for `Card`s, this
is easy, since playing cards are (abstractly) just a pair of suit and rank.
Our operator makes that comparison explicit.

Side note: Unfortunately, we can't (yet) rely on the compiler to generate this 
simple definition for us. There was a proposal to require it, but it hasn't
made it into the language yet, sadly.

For more complex data types like `std::string` or `std::unordered_set`, we need 
to think a little more abstractly. A *string* is a sequence of characters, so
two `std::string`s are equal when they are the same sequences of characters.
A *set* (of values) is a collection of distinct values. For example, the set
${1, 2, 2}$ and the set ${1, 2}$ are equal. The data structure 
`std::unordered_set`s represents these abstractions using a hash table.
An efficient algorithm for determining when two hash tables have the same
elements is quite complicated.

That said, there are a few other axioms related to equality. Any type for
which you define `operator==`, must be

- reflexive: `a == a`,
- symmetric: if `a == b` then `b == a`, and
- transitive: if `a == b` and `b == c` then `a == c`.

In other words, `operator==` must be an equivalence relation. If you implement
`operator==` in a way that violates these axioms, then any use of your data
type will produce unexpected and therefore undefined results.

Side note: Again, axioms really specify sets of values. It is entirely 
reasonable to define a data type with exception values (e.g., floating point 
NaN) for which these axioms do not hold. In cases were equality comparison is 
required, the use of NaNs would yield undefined behaviors. As a specific
example, trying to `std::find` and NaN value within a sequence will always
fail since NaN values are incomparable to any other value (including itself!).
This breaks the guarantees of the `std::find` algorithm.

Incidentally, if you provide an `operator==`, then you had also better provide
an `operator!=`. Those operators come in pairs. After all, if you can tell
when two objects are equal, then you can also tell when they're different,
and you can do it simply by writing `!(a == b)`. In fact, this is by far the
most common implementation of that function:

    bool operator!=(Card a, Card b) {
        return !(a == b);
    }

We could have written this function by applying DeMorgan's law to the original
definition, but this is easier.

### Style note: equality

I've defined our equality comparison operators as non-member functions. There
are a few other ways to write this:

- as member functions or
- as friend functions.

Most overloadable operators can be written either as non-member functions (like
we did previously) or as member functions. Here is a snippet of `Card` where 
the equality overloads as defined as member functions.

    class Card {
    public:
        // ...
        bool operator==(Card that) const {
            return rank == that.rank && suit == that.suit;
        }
        bool operator!=(Card that) const {
            return !(*this == that);
        }
        // ...
    };

In these member functions, the `this` object is always the term on left
in the expression `a == b`. The parameter `that` refers to the right-hand
operand. Both member functions are `const` qualified, meaning they will not
change the value of `this` object.

Because we've written these as member functions, we have direct access to
our member variables -- it's our class, of course we have access! As a result,
we don't have to use accessor functions to get the data.

Advanced programming: a downside of this approach is that the first parameter is
passed by reference. For simple objects like `Card`, that may actually affect
performance since one of the objects being compared is not a local variable.
However, that might only be noticeable at extreme scale (e.g., comparing 
millions of cards?). Even still, this is one reason I prefer to write operators 
as non-member functions. You get total control over when objects are passed
by value or reference.

You can also call operator functions by their name. For example, we could have 
defined `operator!=` as `return !operator==(that)`.

An alternative is to define these functions as `friend` functions. Here's what 
that might look like:

    class Card {
    public:
        // ...
        friend bool operator==(Card a, Card b);
        friend bool operator!=(Card a, Card b);
    };

A `friend` function is a non-member function that is granted access to the 
private members of the declaring class. Even though these functions appear
to be declared within the `Card`, they are not actually members of the 
class. A `friend` declaration says, "there is a function named e.g., `operator==`
that can access my private member variables and function variabes." Friend
declarations are also a good source of inappropriate jokes.

Here are the definitions of the functions above.

    bool operator==(Card a, Card b) {
        return a.rank == b.rank && a.suit == b.suit;
    }

    bool operator!=(Card a, Card b) {
        return !(*a == b);
    }

These are just normal functions, except that they refer directly to the private
members of `Card`.

It is quite common to see friend functions defined within a class.

    class Card {
    public:
        // ...
        friend bool operator==(Card a, Card b) {
            return a.rank == b.rank && a.suit == b.suit;

        }
        friend bool operator!=(Card a, Card b) {
            return a.rank == b.rank && a.suit == b.suit;
        }
    };

This is essentially equivalent to the code above, except that the functions
are defined in only one place.

### Ordering values

Having overloaded `==` and `!=`, we should also consider whether it is 
worthwhile to also provide definitions for the ordering operators 
`<`, `>`, `<=`, and `>=`. It is easy, and somewhat common, for programmers
to assume that these *should* exist simply because they *can* exist. However,
it is very easy to define these operators in a way that makes them inconsistent
with their expected meanings.

When deciding whether to provide overloads for the relational operators, you
need to decide if the values represented have a total order. That is, for any
pair values, you can determine which is less than the other, or whether they
are equal. This is called the *law of trichotemy*. Mathematically, given two 
values $a$ and $b$, exactly one of the following holds:

- $a < b$
- $b < a$
- $a = b$

If, for every pair of values represented by your class, this property holds, 
then you should provide overloads of the ordering operators.

Obviously this property holds for integers and for real numbers. It does not,
however, hold for complex numbers.

TODO: Explain why.

This property holds for playing cards. We can choose some way of comparing
cards that tells us which is less.

TODO: Explain how to write this. Explain lexicographical orders in general.

Note that you can apply a lexicographical ordering to complex numbers, but
this is, in some sense, "artificial". Complex numbers have no natural total
order. However, we could still construct a default total order. The C++
implementation `std::complex` does not do this, however.

### Weak orders

A weak order is like a total order, except that the equality statement in
the law of trichotemy is weakened to *equivalence*. 

For a type whose value is "this and this and this", then you get a total
order by lexicographically comparing all its components. If you only order
on some of its components, you get a weak order. This is useful in sorting.

### Partial orders

TODO: Describe partial orders.

## Copying TL;DR

